from os.path import join, dirname, isfile
from os import walk, makedirs, rmdir, remove
from pickle import dump, load
from base64 import urlsafe_b64encode, urlsafe_b64decode


# touch file, creating all intermediate dirs.
def xopen(p, mode, chmod=0o770):
    path = dirname(p)
    if path:
        makedirs(path, chmod, 1)
    return open(p, mode)


# remove file and all intermediate dirs if possible
def xremove(fp):
    remove(fp)
    rmdir(dirname(fp))


class KeyValueStorage:
    def __init__(self, rootdir=""):
        assert rootdir
        self.rootdir = rootdir

    def _get_item_path(self, key):
        uid = urlsafe_b64encode(key.encode()).decode()
        prefixlen = len(uid) // 4
        return join(self.rootdir, uid[:prefixlen], uid[prefixlen:])

    def persist(self, key, item):
        path = self._get_item_path(key)
        with xopen(path, 'wb') as f:
            dump(item, f)

    def exists(self, key):
        path = self._get_item_path(key)
        return isfile(path)

    def load(self, key):
        if not self.exists(key):
            return None

        path = self._get_item_path(key)
        with open(path, 'rb') as f:
            return load(f)

    def remove(self, key):
        if not self.exists(key):
            return

        path = self._get_item_path(key)
        try:
            # FIXME(b1narykid): assumes uid is split into two parts
            # same applies to iter func below
            xremove(path)
        except OSError:
            pass

    def keys(self):
        for root, dirs, _ in walk(self.rootdir):
            for prefix in dirs:
                subdir = join(root, prefix)
                for _, _, files in walk(subdir):
                    for suffix in files:
                        key = urlsafe_b64decode(prefix + suffix).decode()
                        yield key

    def items(self):
        for key in self.keys():
            yield key, self.load(key)

    def __iter__(self):
        return self.keys()
