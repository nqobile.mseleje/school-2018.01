import Pyro4

from .inv_index.search import SearchQueryProcessor


@Pyro4.expose
class NodeService:
    def __init__(self, config):
        self._search_processor = SearchQueryProcessor(config)

    def search(self, query):
        return self._serch_processor(query)
